<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'advstartup');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a*t#yfFp3nKo4!eTs5-2[]j^F2_v%<,3!!,W&~2J)<nxzj?-Y!E QVY3g%N]UN4J');
define('SECURE_AUTH_KEY',  '=.CZS}04/@$2+`xZjX`<e2KUTt,k0g;SQY)N!1ElgO9z|?`=R+c[Im;WeB;`=c F');
define('LOGGED_IN_KEY',    '#:qa}Wn[}*%up6a//<`<qE3($h.2Bub=F4pONb/^::x3O=RCtm&mej0[imm[9lQM');
define('NONCE_KEY',        '6IH6Tt#CbLo/J26pwo$^/f?Tv{SIQ)knjluOjk&1>zxV[!`5nX^sQP1Hax|vQ8Uh');
define('AUTH_SALT',        'dUu2e/ t~j,*(MK!?YURM--OY$Whpcace5ak*&qg3suTt8V@OA]]_pQ|/&f7YwsI');
define('SECURE_AUTH_SALT', 'mGn0u_CGglav)95$WblLVfJeJfc)(;XX$ct_2Zo1kIONA)/5vQ)1x;cV{aGM5NZ5');
define('LOGGED_IN_SALT',   ';2{dEVYBg-,+8O~[r{OW]nt_3j=:g]Ea-$|G6] {2a=09qJ_k?L&}HbRc]Bl@oLL');
define('NONCE_SALT',       '#wwPK?4LG wADXH3%(P_A@qK5X<e<t._ac>^?03|216Hozg&v,hAO U,=g-gWm|+');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'adv_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */
define( 'ALLOW_UNFILTERED_UPLOADS', true );

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
